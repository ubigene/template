# -*- coding: utf-8 -*-
""" Submodule user interface """
import logging

_log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def populate_arg_parser(parser):
    """ Argument parser specification for submodule1
    """
    subparsers = parser.add_subparsers(help='Commands')

    hello = subparsers.add_parser(
        "hello",
        help="Print hello message")
    hello.set_defaults(run_submodule1_mode_="hello")
    hello.add_argument(
        "name", type=str,
        help="User name")


def run(args, _):
    """ submodule1 command line interface implementation """
    if args.run_submodule1_mode_ == "hello":
        print "Hello {name}".format(name=args.name)
    else:
        raise ValueError(
            "Mode {} not supported".format(args.run_submodule1_mode_))
