""" Project user interface entry point """
import argparse
import logging

import py


def populate_arg_parser(parser):
    """ Function to build argument parser for the project
    by including populate_arg_parser() from submodules.
    """
    subparsers = parser.add_subparsers(help='Functions')
    # the list below defines, which submodules to include
    for (name, hlp, module) in [
            ("submodule1", "submodule1 console interface",
             py.submodule1.main)]:
        sub = subparsers.add_parser(name, help=hlp)
        sub.set_defaults(__bin_processor__=module.run)
        module.populate_arg_parser(sub)
    return parser


def main(args):
    """ This is the main application entry point.
    Calls an appropriate submodule's UI entry point depending on user input
    """
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='User interface scripts')
    populate_arg_parser(parser)
    args, unknown_arg = parser.parse_known_args(args)

    call_function = args.__bin_processor__
    return call_function(args, unknown_arg)
