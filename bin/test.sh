#!/bin/bash
# Test runner script

# source execution context
source $(dirname $0)/env.sh

# activate virtualenv
if [ ! -d "${ENV_PATH}" ]; then
    echo "Error: virtualenv not found, please run init.sh"
    exit 1
fi 
source "$ENV_PATH/bin/activate"

# run python scripts
nosetests "$@"
