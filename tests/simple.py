# -*- coding: utf-8 -*-
""" Simple tests """
from unittest import TestCase


class SimpleTest(TestCase):
    """ Simple test class """
    def test_always_true(self):
        """ Test that always passes """
        self.assertTrue(True)   # pylint: disable=redundant-unittest-assert
